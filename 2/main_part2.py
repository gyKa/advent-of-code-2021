input_file = open("input.txt", "r")
steps = input_file.readlines()

coordinates = []

for step in steps:
    step = step.strip()
    direction, position = step.split()
    coordinates.append((direction, int(position)))

horizontal = 0
depth = 0
aim = 0

for cordinate in coordinates:
    print(cordinate)

    if cordinate[0] == 'forward':
        horizontal += cordinate[1]
        depth += (aim * cordinate[1])
        # print(f"deph: {depth}, aim: {aim}, x: {cordinate[1]}")
        continue

    if cordinate[0] == 'up':
        # depth -= cordinate[1]
        aim -= cordinate[1]
        continue

    if cordinate[0] == 'down':
        # depth += cordinate[1]
        aim += cordinate[1]

print(horizontal * depth)
