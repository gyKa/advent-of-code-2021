with open("input.txt", "r", encoding="utf-8") as f:
    numbers = f.readline().strip().split(",")
    boards_input = f.read().splitlines()

boards = []
current_board = []

for boards_line in boards_input:
    if boards_line == "":
        if current_board:
            boards.append(current_board)

        current_board = []

        continue

    row = boards_line.split()
    row = [int(i) for i in row]
    current_board.append(row)

boards.append(current_board)

#########################################################################################

won_board = None

drawn_numbers = []
# numbers = [7]
won_boards = []
won_drawn_number = 0
won_drawn_number_index = 0

for number_index, number in enumerate(numbers):
    print(
        "----------------------------------------------------------------------------"
    )
    print(f"WON BOARDS: {won_boards}")
    # if won_board:
    # won_boards.append(won_board)

    drawn_numbers.append(int(number))

    # print()
    print(f"drawn number {number}, drawn numbers: {drawn_numbers}")
    print()

    for board_id, board in enumerate(boards):
        if board_id in won_boards:
            continue

        print(f"Board ID: {board_id} board: {board}")

        # check rows
        for row in board:
            print(f"checking row {row}")
            # if won_board:
            # break

            marked_numbers = 0

            for numb in row:
                if numb in drawn_numbers:
                    marked_numbers += 1

            print(f"row marked numbers count: {marked_numbers}")

            if marked_numbers == 5:
                won_board = board_id
                print("found winning row, exiting")
                break

        if won_board is not None:
            print(f"row: adding {won_board} to winning boards by drawn number {number}")
            won_boards.append(won_board)
            won_drawn_number = number
            won_drawn_number_index = number_index
            won_board = None
            continue

        # check columns
        for column in range(5):
            print(f"checking column {column}")

            marked_numbers = 0

            for row in board:
                if row[column] in drawn_numbers:
                    marked_numbers += 1

            print(f"column marked numbers count: {marked_numbers}")

            if marked_numbers == 5:
                won_board = board_id
                print(f"found winning column, exiting")
                break

        if won_board is not None:
            print(
                f"column: adding {won_board} to winning boards by drawn number {number}"
            )
            won_boards.append(won_board)
            won_drawn_number = number
            won_drawn_number_index = number_index
            won_board = None

    if len(boards) == len(won_boards):
        print("ALL BOARDS WON")
        break

#########################################################################################
print("CALCULATING score...")
score = 0

print()
print(f"{won_boards}")
print(f"draw index {won_drawn_number_index}")
print(f"draw number {won_drawn_number}")

for row in boards[won_boards[-1]]:
    for numb in row:
        if numb not in drawn_numbers:
            score += numb

print(f"WON BOARD #{board_id} by number {drawn_numbers[-1]}: {boards[board_id]}")
print(f"score {score} is {score * drawn_numbers[-1]}")
