with open("input.txt", "r", encoding="utf-8") as f:
    numbers = f.readline().strip().split(",")
    boards_input = f.read().splitlines()

boards = []
current_board = []

for boards_line in boards_input:
    if boards_line == "":
        if current_board:
            boards.append(current_board)

        current_board = []

        continue

    row = boards_line.split()
    row = [int(i) for i in row]
    current_board.append(row)

boards.append(current_board)

#########################################################################################
won_board = None

drawn_numbers = []
# numbers = [7]

for number in numbers:
    if won_board:
        break

    drawn_numbers.append(int(number))

    print()
    print(f"drawn number {number}, drawn numbers: {drawn_numbers}")
    print()

    for board_id, board in enumerate(boards):
        if won_board:
            break

        print(board_id, board)

        # check rows
        for row in board:
            print(f"checking row {row}")
            # if won_board:
            # break

            marked_numbers = 0

            for numb in row:
                if numb in drawn_numbers:
                    marked_numbers += 1

            print(f"row marked numbers count: {marked_numbers}")

            if marked_numbers == 5:
                won_board = board_id
                print("found winning row, exiting")
                break

        if won_board:
            break

        # check columns
        for column in range(5):
            print(f"checking column {column}")

            marked_numbers = 0

            for row in board:
                if row[column] in drawn_numbers:
                    marked_numbers += 1

            print(f"column marked numbers count: {marked_numbers}")

            if marked_numbers == 5:
                won_board = board_id
                print("found winning column, exiting")
                break

#########################################################################################

score = 0

for row in boards[board_id]:
    for numb in row:
        if numb not in drawn_numbers:
            score += numb

print(f"WON BOARD #{board_id} by number {drawn_numbers[-1]}: {boards[board_id]}")
print(f"score {score} is {score * drawn_numbers[-1]}")
