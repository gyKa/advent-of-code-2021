def eliminate(s, l):
    remove_value = []

    for value in l:
        if not value.startswith(s, 0, len(s)):
            remove_value.append(value)

    for i in remove_value:
        l.remove(i)

    return l


def calc_stats(numbers: list):
    stats = []
    binary_number_length = 0

    if numbers:
        binary_number_length = len(numbers[0])

        for i in range(binary_number_length):
            stats.append({'0': 0, '1': 0})

    for binary_number in numbers:
        for index in range(binary_number_length):
            if binary_number[index] == '0':
                stats[index]['0'] = stats[index]['0'] + 1
            elif binary_number[index] == '1':
                stats[index]['1'] = stats[index]['1'] + 1

    return stats


########################################################################################

with open("input.txt", "r") as f:
    binary_numbers = f.read().splitlines()

oxygen_binary_numbers = binary_numbers.copy()
co2_binary_numbers = binary_numbers.copy()

#########################################################################################

oxygen_string = ''

bit_number = 1

while True:
    if len(oxygen_binary_numbers) == 1:
        break

    stats = calc_stats(oxygen_binary_numbers)
    loop_stats = stats[bit_number - 1]

    if loop_stats['0'] < loop_stats['1']:
        oxygen_string += '1'
    elif loop_stats['0'] > loop_stats['1']:
        oxygen_string += '0'
    else:
        oxygen_string += '1'
    
    oxygen_binary_numbers = eliminate(oxygen_string, oxygen_binary_numbers)

    bit_number += 1

print(oxygen_binary_numbers)

#########################################################################################

co2_string = ''

bit_number = 1

while True:
    if len(co2_binary_numbers) == 1:
        break

    stats = calc_stats(co2_binary_numbers)

    loop_stats = stats[bit_number - 1]

    if loop_stats['0'] < loop_stats['1']:
        co2_string += '0'
    elif loop_stats['0'] > loop_stats['1']:
        co2_string += '1'
    else:
        co2_string += '0'
    
    co2_binary_numbers = eliminate(co2_string, co2_binary_numbers)

    bit_number += 1

print(co2_binary_numbers)

#########################################################################################

print(int(oxygen_binary_numbers[0], 2) * int(co2_binary_numbers[0], 2))
