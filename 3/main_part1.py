input_file = open("input.txt", "r")
binary_numbers = input_file.readlines()

stats = {}

for binary_number in binary_numbers:
    binary_number = binary_number.strip()

    length = len(binary_number)

    for index in range(length):
        if index not in stats:
            stats[index] = {'0': 0, '1': 0}

        if binary_number[index] == '0':
            stats[index]['0'] = stats[index]['0'] + 1
        elif binary_number[index] == '1':
            stats[index]['1'] = stats[index]['1'] + 1

print(stats)

gamma = ''
epsilon = ''

for key, value in stats.items():
    if value['0'] > value['1']:
        gamma += '0'
        epsilon += '1'

    if value['1'] > value['0']:
        gamma += '1'
        epsilon += '0'

print(gamma)
print(epsilon)
print(int(gamma, 2))
print(int(epsilon, 2))
print(int(gamma, 2) * int(epsilon, 2))
