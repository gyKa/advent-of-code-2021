input_file = open("input.txt", "r")
measurements = input_file.readlines()

previous_measurement = None
count = 0

for measurement in measurements:
    current_measurement = int(measurement.strip())

    if previous_measurement is None:
        print(f"{current_measurement} N/A - no previous measurement")
    elif previous_measurement > current_measurement:
        print(f"{current_measurement} (decreased)")
    elif previous_measurement == current_measurement:
        print(f"{current_measurement} (no change)")
    elif previous_measurement < current_measurement:
        print(f"{current_measurement} (increased)")
        count += 1

    previous_measurement = current_measurement

print(f"\n{count}")
