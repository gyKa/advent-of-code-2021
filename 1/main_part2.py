input_file = open("input.txt", "r")
measurements = input_file.readlines()

measurements_cleaned = []

for measurement in measurements:
    measurements_cleaned.append(int(measurement.strip()))

###
slices_of_measurements = []
measurements_count = len(measurements_cleaned)

for index, _ in enumerate(measurements_cleaned):
    limit = index + 3

    if limit > measurements_count:
        limit = measurements_count

    slice_of_measurements = measurements_cleaned[index:limit]
    slices_of_measurements.append(slice_of_measurements)

###
previous_measurement = None
count = 0

for s in slices_of_measurements:
    current_measurement = sum(s)

    if previous_measurement is None:
        print(f"{current_measurement} N/A - no previous measurement")
    elif previous_measurement > current_measurement:
        print(f"{current_measurement} (decreased)")
    elif previous_measurement == current_measurement:
        print(f"{current_measurement} (no change)")
    elif previous_measurement < current_measurement:
        print(f"{current_measurement} (increased)")
        count += 1

    previous_measurement = current_measurement

print(f"\n{count}")
